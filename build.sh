
#cd interpreter
#flex --outfile=lexer.c --header-file=lexer.h lexer.l
#bison --output-file=parser.c --defines=parser.h --warnings=all --feature=all parser.y
#cd ..

rm -rf _b
mkdir _b
cd _b
cmake \
	-DCMAKE_C_COMPILER=gcc \
	-DCMAKE_CXX_COMPILER=g++ \
	-DCMAKE_C_FLAGS="-fPIC" \
	-DCMAKE_CXX_FLAGS="-fPIC" \
	-DCMAKE_INSTALL_PREFIX=_i \
	-DBUILD_SHARED_LIBS=0 \
	..

make install
#ninja install/strip

#-GNinja \
#-DCMAKE_EXE_LINKER_FLAGS="-fuse-ld=lld" \
#	-DCMAKE_C_FLAGS="-fPIC -Wall -Wpedantic" \
#	-DCMAKE_CXX_FLAGS="-fPIC -Wall -Wpedantic" \

