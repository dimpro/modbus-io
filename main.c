
#include "accessor/accessor.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "interpreter/interpreter.h"
#include "transport/transport.h"
#ifdef __cplusplus
}
#endif

#include <stdio.h>
#include <string.h>

void print_ans(unsigned int uri_id)
{
    switch (regmap_array[uri_id]->uri_value.val_type)
    {
    case variant_int8: printf("%d", regmap_array[uri_id]->uri_value.val.val_int8); break;
    case variant_uint8: printf("%u", regmap_array[uri_id]->uri_value.val.val_uint8); break;
    case variant_int16: printf("%d", regmap_array[uri_id]->uri_value.val.val_int16); break;
    case variant_uint16: printf("%u", regmap_array[uri_id]->uri_value.val.val_uint16); break;
    case variant_int32: printf("%d", regmap_array[uri_id]->uri_value.val.val_int32); break;
    case variant_uint32: printf("%u", regmap_array[uri_id]->uri_value.val.val_uint32); break;
    case variant_int64: printf("%ld", regmap_array[uri_id]->uri_value.val.val_int64); break;
    case variant_uint64: printf("%lu", regmap_array[uri_id]->uri_value.val.val_uint64); break;
    case variant_float: printf("%f", (double)regmap_array[uri_id]->uri_value.val.val_float); break;
    case variant_double: printf("%f", regmap_array[uri_id]->uri_value.val.val_double); break;
    default:;
    }
}

void read_slave_device(unsigned int uri_id)
{
    uint8_t tx[100], tx_size, rx[100], rx_size;

    modbus_pack_t modbus_pack;
    memset(&modbus_pack, 0, sizeof(modbus_pack_t));
    modbus_pack.addr_device = 3;
    modbus_pack.func = READ_HOLDING_REGISTERS;
    modbus_pack.start_addr = (uint16_t)regmap_array[uri_id]->register_base;
    modbus_pack.quantity = (uint16_t)regmap_array[uri_id]->register_count;
    pack_message(MODBUS_REQUEST, &modbus_pack, tx, &tx_size);

    add_crc(tx, &tx_size);

    int fd = transport_serial_port_open("/dev/ttyUSB0", 19200);
    transport_write(fd, tx, tx_size);
    rx_size = (uint8_t)transport_read(fd, rx, 100);

    printf("readed %u: ", rx_size);
    for (int i = 0; i < rx_size; ++i)
        printf("%02X ", rx[i]);
    printf("\n");

    if (!check_crc(rx, rx_size))
    {
        printf("CRC ERR\n");
    }
    else
    {
        unpack_message(MODBUS_RESPONSE, &modbus_pack, rx, NULL);
        if (modbus_pack.err)
        {
            printf("MODBUS ERR\n");
        }
        else
        {
            for (unsigned int i = 0; i < regmap_array[uri_id]->register_count; ++i)
            {
                memcpy(&regmap_array[uri_id]->register_value[i].val.val_uint16, modbus_pack.value + i * 2, 2);
            }
            calc_from_reg(uri_id);
            printf("calculated value for accessor: ");
            print_ans(uri_id);
            printf("\n");
        }
    }
}

int main(int argc, char ** argv)
{
    if (argc < 2) return 1;
    if (parse_cfg(argv[1]))
        return 1;
//for work from qtcreator
//    if (parse_cfg("../example_cfg"))
//        return 1;

    for (unsigned int i = 0; i < regmap_count; ++i)
    {
        //здесь надо открыть хендлы на каждый uri
        //формат uri должен знать только accessor
        //open_handle_for_uri(regmap_array[i]->uri);
    }

    //прочитать регистры указанные в файле конфигурации как холдинг регистры из слэйв устройства
    //регистры будут вычитаны по интерфейсу и пересчитаны в соответствии с файлом конфигурации
    for (unsigned int i = 0; i < regmap_count; ++i)
    {
        read_slave_device(i);
    }

    cfg_free();

    return 0;
}
