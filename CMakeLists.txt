
cmake_minimum_required(VERSION 2.8)

project(modbus-io)

include_directories(${PROJECT_SOURCE_DIR})

find_package(FLEX REQUIRED)
set(FlexFilePref ${PROJECT_SOURCE_DIR}/interpreter/lexer)
if(FLEX_FOUND)
    execute_process(
        COMMAND ${FLEX_EXECUTABLE}
            --outfile=${FlexFilePref}.c
            --header-file=${FlexFilePref}.h
            #--debug
            ${FlexFilePref}.l
        )
endif()

find_package(BISON REQUIRED)
set(BisonFilePref ${PROJECT_SOURCE_DIR}/interpreter/grammar)
if(BISON_FOUND)
    execute_process(
        COMMAND ${BISON_EXECUTABLE}
            --output-file=${BisonFilePref}.c
            --defines=${BisonFilePref}.h
            --warnings=all
            --feature=all
            #--debug
            ${BisonFilePref}.y
        )
endif()
    
set(libs_list)
function(user_add_library lib_name)
    message("lib: ${lib_name}")
    aux_source_directory(${lib_name} ${lib_name}_src)
    add_library(${lib_name} ${${lib_name}_src})
    set(libs_list ${libs_list} ${lib_name} PARENT_SCOPE)
endfunction()

message("===> modbus-io libraries: ")
user_add_library(modbus_packer)
user_add_library(crc16)
user_add_library(variant)
user_add_library(interpreter)

if ( NOT DEFINED TARGET_PLATFORM )
    aux_source_directory(${PROJECT_SOURCE_DIR} main_src)
    add_executable(${CMAKE_PROJECT_NAME} ${main_src})
    target_link_libraries(${CMAKE_PROJECT_NAME} ${libs_list})

    install(
        TARGETS
        modbus-io
        DESTINATION bin)

    if(BUILD_SHARED_LIBS)
        install(
            TARGETS
            modbus_packer
            crc16
            variant
            interpreter
            DESTINATION lib/${CMAKE_PROJECT_NAME})

        install(
            FILES
            modbus_packer/modbus_packer.h
            crc16/crc16.h
            variant/variant.h
            interpreter/interpreter.h
            DESTINATION include/${CMAKE_PROJECT_NAME})
    endif()
endif()
