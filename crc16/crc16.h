
#ifndef _CRC16_H_
#define _CRC16_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t check_crc(uint8_t *buffer, uint8_t size);
void add_crc(uint8_t *buffer, uint8_t *size);

#ifdef __cplusplus
}
#endif

#endif //_CRC16_H_
