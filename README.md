
# modbus-io

### build
in project directory
```sh
./build.sh
```

### start
```sh
_b/_i/bin/modbus-io example_cfg
```

it's possible to work from qtcreator
for it need to change argv[1] to another value
in my case it was "../example_cfg"

