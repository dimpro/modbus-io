
#include <stdio.h>

#include "interpreter.h"
#include "lexer.h"
#include "grammar.h"
#include "parser.h"
#include "variant/variant.h"

#ifdef YYDEBUG
  int yydebug = 1;
#endif

extern parser_calc_element_t ** regmap_calc_from_start;
extern parser_calc_element_t ** regmap_calc_to_start;

int yyerror(yyscan_t scanner, char *msg)
{
    fprintf(stderr, "parse error: %s\n", msg);
    parser_free();
    return 0;
}

void cfg_free(void)
{
    parser_free();
}

int parse_cfg(const char * filepath)
{
    FILE * fd;
    fd = fopen(filepath, "rb");
    if (fd == 0)
    {
        return 1;
    }

    yyscan_t scanner;
    yylex_init(&scanner);
#ifdef FLEX_DEBUG
    yyset_debug(1, scanner);
#endif
    yyset_in(fd, scanner);
    parser_clear_error();
    int res = yyparse(scanner);
    yylex_destroy(scanner);

    fclose(fd);

    int e = parser_iserror();
    if (e)
    {
        printf("E:%d\n", e);
        yyerror(NULL, "cfg err");
    }

    return res | e;
}

void print_calc(parser_calc_element_t * p)
{

    printf(" [%d] ", p->argc);
    printf("=%d%c<%d> ",
           variant_dereference(&p->ret)->val.val_int32,
           p->ret.val_p ? '*' : ' ',
           variant_dereference(&p->ret)->val_type
    );
    for (unsigned int i = 0; i < p->argc; ++i)
        printf("%d%c ",
               variant_dereference(p->argv[i])->val.val_int32,
               p->argv[i]->val_p ? '*':' '
        );
    printf("\n");
}

void calc_from_reg(unsigned int uri_id)
{
    printf("calc from regs:\n");
    parser_calc_element_t * p = regmap_calc_from_start[uri_id];
    while (p)
    {
        //printf("  ");
        //print_calc(p);
        //printf(" [%d] ", p->argc);
        p->func(p);
        print_calc(p);

        p = p->next_element;
    }
}

void calc_to_reg(unsigned int uri_id)
{
    printf("calc to regs:\n");
    parser_calc_element_t * p = regmap_calc_to_start[uri_id];
    while (p)
    {
        //printf("  ");
        //print_calc(p);
        p->func(p);
        print_calc(p);

        p = p->next_element;
    }
}

