
#ifndef _INTERPRETER_H_
#define _INTERPRETER_H_

#include <stdint.h>
//#include <stdlib.h>
#include "variant/variant.h"

#ifdef __cplusplus
extern "C" {
#endif

//#pragma pack(push, 1)
//#pragma pack(pop)

struct regmap_t
{
    char * uri;
    char **params;
    variant_t uri_value;
    unsigned int register_base;
    unsigned int register_count;
    unsigned char function_code;
    variant_t register_value[];
};

extern unsigned int regmap_count;
extern struct regmap_t ** regmap_array;

int parse_cfg(const char * filepath);
void calc_from_reg(unsigned int uri_id);
void calc_to_reg(unsigned int uri_id);
void cfg_free(void);

#ifdef __cplusplus
}
#endif

#endif //_INTERPRETER_H_
