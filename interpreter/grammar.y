
//for c++
//    add %skeleton "lalr1.cc"
//    remove api.pure
//    remove yyerror
//%verbose
//%error-verbose
%define parse.error verbose
//%define api.pure full
//%define api.pure
%locations
//%token-table
//%glr-parser
%lex-param {void * scanner}
%parse-param {void * scanner}
//%lex-param {yyscan_t scanner}
//%parse-param {yyscan_t scanner}
%define api.value.type {lval_t}
//%define api.value.type {void *}

%code requires
{
#include "parser.h"
}

%{
#include <stdio.h>
#include "grammar.h"
#include "lexer.h"
//void yyerror(YYLTYPE *, yyscan_t, char const *);
void yyerror(yyscan_t, char const *);
%}

%token EOL SHL SHR NUM_PWR
%token LONG HEXLONG DOUBLE NAME STR

%start input

%%

input
    : %empty
    | input any_str EOL
    | input EOL
    ;
any_str
    : regmap_str
    | calc_from_str
    | calc_to_str
    ;

str
    : STR { $$.s = strdup($1.s); free($1.s); $1.s = NULL; }
    ;

regmap_str
    : LONG ':' LONG ';' NAME ';' STR ';' NAME { parser_regmap_add(&$1, &$3, $5.s, $7.s, $9.s); }
    ;
calc_from_str
    : '>' str ';' expr { parser_calc_from_link($2.s, &$4); }
    ;
calc_to_str
    : '<' str ';' exprs { parser_calc_to_link($2.s, &$4); }
    ;

exprs
    : expr { $$.calc_element = NULL; parser_calc_element_append(&$$, &$1); }
    | exprs ';' expr { parser_calc_element_append(&$1, &$3); $$ = $1; }
    ;

foo_exprs
    : %empty { $$.calc_element = NULL; parser_calc_element_append(&$$, NULL); }
    | foo_exprs_ {$$ = $1;}
    ;
foo_exprs_
    : expr { $$.calc_element = NULL; parser_calc_element_append(&$$, &$1); }
    | foo_exprs_ ',' expr { parser_calc_element_append(&$1, &$3); $$ = $1; }
    ;
name
    : NAME { $$.s = strdup($1.s); free($1.s); $1.s = NULL; }
    ;
foo
    : name '(' foo_exprs ')' { $$ = parser_func_element_foo($1.s, &$3); }
    ;

expr
    : expr0 {$$ = parser_calc_element_end(&$1);}
expr0
    : expr1 {$$ = $1;}
    | expr0 '+' expr1 {$$ = parser_calc_element_op(calc_add, &$1, &$3);}
    | expr0 '-' expr1 {$$ = parser_calc_element_op(calc_minus, &$1, &$3);}
    ;
expr1
    : expr2 {$$ = $1;}
    | expr1 '*' expr2 {$$ = parser_calc_element_op(calc_mul, &$1, &$3);}
    | expr1 '/' expr2 {$$ = parser_calc_element_op(calc_div, &$1, &$3);}
    | expr1 '%' expr2 {$$ = parser_calc_element_op(calc_mod, &$1, &$3);}
    ;
expr2
    : expr3 {$$ = $1;}
    //| expr2 NUM_PWR expr3 {}
    ;
expr3
    : expr4 {$$ = $1;}
    | expr3 '&' expr4 {$$ = parser_calc_element_op(calc_and, &$1, &$3);}
    | expr3 '|' expr4 {$$ = parser_calc_element_op(calc_or, &$1, &$3);}
    | expr3 '^' expr4 {$$ = parser_calc_element_op(calc_xor, &$1, &$3);}
    ;
expr4
    : element {$$ = $1;}
    | expr4 SHL element {$$ = parser_calc_element_op(calc_shl, &$1, &$3);}
    | expr4 SHR element {$$ = parser_calc_element_op(calc_shr, &$1, &$3);}
    ;

element
    : '(' expr0 ')' { $$ = $2; }
    | foo { $$ = $1; }
    | unary_long { $$ = $1; }
    | HEXLONG { $$ = $1; }
    | unary_double { $$ = $1; }
    | name { $$ = parser_add_variable($1); }
    ;
unary_long
    : LONG { $$ = $1; }
    | '-' LONG { $1.variant->val.val_int64 *= -1; $$ = $1; }
    ;
unary_double
    : DOUBLE { $$ = $1; }
    | '-' DOUBLE { $1.variant->val.val_double *= -1; $$ = $1; }
    ;
%%
