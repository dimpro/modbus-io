
#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "interpreter.h"

#define parser_printf(...) printf(__VA_ARGS__)


unsigned int regmap_count = 0;
struct regmap_t ** regmap_array;
parser_calc_element_t ** regmap_calc_from_start;
parser_calc_element_t ** regmap_calc_to_start;

//#pragma pack(push, 1)
struct variable_descriptor_t
{
    char * var_name;
    variant_t * var_pointer;
};
//#pragma pack(pop)

static unsigned int variable_count = 0;
static struct variable_descriptor_t * variable_list;

static parser_calc_element_t * calc_element_first = NULL;
static parser_calc_element_t * calc_element_last = NULL;

static int error;

void parser_clear_error()
{
    error = 0;
}

int parser_iserror()
{
    return error;
}

int memory_allocated_count = 0;

void parser_inc_allocated_count(const char * desc)
{
    memory_allocated_count++;
    parser_printf("memory +1 = %d (%s)\n", memory_allocated_count, desc);
}

void parser_dec_allocated_count()
{
    memory_allocated_count--;
    parser_printf("memory -1 = %d\n", memory_allocated_count);
}

void * parser_alloc(void * p, size_t size)
{
    unsigned char isnew = 0;
    if (p == NULL)
        isnew = 1;

    void * ret = realloc(p, size);
    if (ret == NULL)
    {
        parser_printf("ERR: memory not allocated\n");
        ret = realloc(p, size);
        if (ret == NULL)
        {
            parser_printf("ERR: memory not allocated again\n");
            return NULL;
        }
    }
    if (isnew)
    {
        memset(ret, 0, size);
        parser_inc_allocated_count("alloc");
    }

    return ret;
}

#define parser_safe_free(pointer) parser_safe_free_((void**)pointer)
void parser_safe_free_(void ** p)
{
    if (!*p)
    {
        parser_printf("ERR: try to free NULL pointer\n");
        return;
    }
    free(*p);
    *p = NULL;

    parser_dec_allocated_count();
}

void parser_free_calc(parser_calc_element_t * start)
{
    parser_printf("free_calc\n");
    parser_calc_element_t *next, *p = start;
    while (p)
    {
        parser_printf("free p->argc: (%d)\n", p->argc);
        for (unsigned int i = 0; i < p->argc; ++i)
        {
            //parser_printf("%d ", p->argv[i]->val.val_int32);
            parser_safe_free(&p->argv[i]);
        }
        next = p->next_element;
        parser_safe_free(&p);
        p = next;
    }
}

void parser_free_variables()
{
    parser_printf("free_variables variable_count: %d\n", variable_count);
    for (unsigned int i = 0; i < variable_count; ++i)
    {
        parser_printf("free var name: %s\n", variable_list[i].var_name);
        parser_safe_free(&variable_list[i].var_name);
    }
    parser_safe_free(&variable_list);
    variable_count = 0;
}

void parser_free_regmap()
{
    parser_printf("free_regmap regmap_count: %d\n", regmap_count);
    for (unsigned int i = 0; i < regmap_count; ++i)
    {
        parser_printf("uri: %s\n", regmap_array[i]->uri);
        parser_safe_free(&regmap_array[i]->uri);
        parser_safe_free(&regmap_array[i]);
        parser_printf("free regmap_calc_from_start\n");
        parser_free_calc(regmap_calc_from_start[i]);
        regmap_calc_from_start[i] = NULL;
        parser_printf("free regmap_calc_to_start\n");
        parser_free_calc(regmap_calc_to_start[i]);
        regmap_calc_to_start[i] = NULL;
    }
    parser_safe_free(&regmap_calc_from_start);
    parser_safe_free(&regmap_calc_to_start);
    parser_safe_free(&regmap_array);
    regmap_count = 0;
}

void parser_free()
{
    parser_printf("free all\n");
    parser_free_calc(calc_element_first);
    calc_element_first = NULL;
    calc_element_last = NULL;

    parser_free_variables();
    parser_free_regmap();
    parser_printf("free all complete\n");
}

//------------------------------------------------------------------
lval_t parser_make_int(char * str)
{
    lval_t lval;
    lval.lval_type = lval_var;
    lval.variant = variant_new();
    parser_inc_allocated_count("make int");
    lval.variant->val.val_int64 = atoll(str);
    lval.variant->val_type = variant_int64;
    return lval;
}

lval_t parser_make_uint(char * str)
{
    lval_t lval;
    lval.lval_type = lval_var;
    lval.variant = variant_new();
    parser_inc_allocated_count("make uint");
    lval.variant->val.val_uint64 = strtoull(str, NULL, 16);
    lval.variant->val_type = variant_uint64;
    return lval;
}

lval_t parser_make_double(char * str)
{
    lval_t lval;
    lval.lval_type = lval_var;
    lval.variant = variant_new();
    parser_inc_allocated_count("make double");
    lval.variant->val.val_double = atof(str);
    lval.variant->val_type = variant_double;
    return lval;
}

lval_t parser_make_str(char * str)
{
    lval_t lval;
    lval.s = strdup(str);
    parser_inc_allocated_count("make str");
    return lval;
}

//------------------------------------------------------------------

void parser_calc_element_new(parser_calc_element_t ** calc_element, unsigned int argc)
{
    int isnew = *calc_element == NULL;
    parser_printf("calc_element_new isnew: %d argc: %d\n", isnew, argc);
    //*calc_element = realloc(*calc_element, sizeof(parser_calc_element_t) + sizeof(variant_t *) * argc);
    *calc_element = parser_alloc(*calc_element, sizeof(parser_calc_element_t) + sizeof(variant_t *) * argc);
    (*calc_element)->argc = argc;
    if (isnew)
        (*calc_element)->next_element = NULL;
}

void parser_calc_element_sequence_add(parser_calc_element_t * calc_element)
{
    parser_printf("calc_element_sequence_add\n");
    if (calc_element_first == NULL)
    {
        parser_printf("first in sequence\n");
        calc_element_first = calc_element;
    }

    if (calc_element_last)
        calc_element_last->next_element = calc_element;

    calc_element_last = calc_element;
}

void parser_calc_element_set_arg(variant_t ** arg, lval_t * val)
{
    parser_printf("calc_element_set_arg\n");
    if (val->lval_type == lval_var)
    {
        parser_printf("lval_var\n");
        (*arg) = val->variant;
    }
    else
    {
        parser_printf("lval_elem\n");
        (*arg) = variant_new();
        parser_inc_allocated_count("set arg");
        parser_printf("argc %d\n", val->calc_element->argc);
        (*arg)->val_p = &(val->calc_element->ret);
    }
}

int parser_check_uri_existance(char * uri)
{
    parser_printf("check_uri_existance: %s\n", uri);
    for (unsigned int i = 0; i < regmap_count; ++i)
    {
        if (strcmp(uri, regmap_array[i]->uri) == 0)
            return 1;
    }
    return 0;
}

static char * parser_type_str_map[variant_type_count] =
{
    "int8",
    "uint8",
    "int16",
    "uint16",
    "int32",
    "uint32",
    "int64",
    "uint64",
    "float",
    "double"
};

int parser_get_type_by_str(char * str)
{
    for (int i = 0; i < variant_type_count; ++i)
    {
        if (strcmp(str, parser_type_str_map[i]) == 0)
            return i;
    }
    return -1;
}

void parser_regmap_add(lval_t * basereg, lval_t * count, char * regtype, char * uri, char * uritype)
{
    unsigned int basereg_i = basereg->variant->val.val_uint32;
    unsigned int count_i = count->variant->val.val_uint32;
    parser_printf("regmap_add uri: %s basereg: %u count: %u\n", uri, basereg_i, count_i);
    parser_safe_free(&basereg->variant);
    parser_safe_free(&count->variant);
    int regtype_i = parser_get_type_by_str(regtype);
    int uritype_i = parser_get_type_by_str(uritype);
    parser_safe_free(&regtype);
    parser_safe_free(&uritype);

    if (parser_check_uri_existance(uri)) error = 3;
    if (regtype_i < 0) error = 3;
    if (uritype_i < 0) error = 3;
    switch (count_i)
    {
    case 0: if (basereg_i != 0) error = 3; break;
    case 1: break;
    case 2: break;
    case 4: break;
    default: error = 3;
    }

    if (!error)
    {
        //struct regmap_t * rmp = malloc(sizeof(struct regmap_t) + sizeof(variant_t) * count);
        struct regmap_t * rmp = NULL;
        rmp = parser_alloc(rmp, sizeof(struct regmap_t) + sizeof(variant_t) * count_i);
        rmp->uri = uri;
        rmp->register_base = basereg_i;
        rmp->register_count = count_i;
        variant_zero(&rmp->uri_value);
        rmp->uri_value.val_type = (enum variant_type_t)uritype_i;
        for (unsigned int i = 0; i < count_i; ++i)
        {
            variant_zero(&rmp->register_value[i]);
            rmp->register_value[i].val_type = (enum variant_type_t)regtype_i;
        }

        if (regmap_count == 0)
        {
            regmap_array = NULL;
            regmap_calc_from_start = NULL;
            regmap_calc_to_start = NULL;
        }
        regmap_count++;
        regmap_array = parser_alloc(regmap_array, sizeof(struct regmap_t *) * regmap_count);
        regmap_array[regmap_count - 1] = rmp;

        regmap_calc_from_start = parser_alloc(regmap_calc_from_start, sizeof(parser_calc_element_t *) * regmap_count);
        regmap_calc_to_start = parser_alloc(regmap_calc_to_start, sizeof(parser_calc_element_t *) * regmap_count);
        //regmap_calc_from_start = realloc(regmap_calc_from_start, sizeof(parser_calc_element_t *) * regmap_count);
        //regmap_calc_to_start = realloc(regmap_calc_to_start, sizeof(parser_calc_element_t *) * regmap_count);
    }
}

void parser_calc_from_link(char * uri, lval_t * val)
{
    parser_printf("calc_from_link uri: %s\n", uri);
    int ok = 0;
    unsigned int found_iter = 0;
    for (unsigned int i = 0; i < regmap_count; ++i)
    {
        if (strcmp(uri, regmap_array[i]->uri) == 0)
        {
            ok = 1;
            found_iter = i;
            break;
        }
    }

    if (ok)
    {
        lval_t lval;
        lval.lval_type = lval_elem;
        lval.calc_element = NULL;
        parser_calc_element_new(&lval.calc_element, 1);
        parser_calc_element_sequence_add(lval.calc_element);
        parser_calc_element_set_arg(&lval.calc_element->argv[0], val);
        lval.calc_element->func = calc_cast_type;

        lval.calc_element->ret.val_p = &regmap_array[found_iter]->uri_value;

        parser_printf("link variables variable_count: %d register_count: %d\n", variable_count, regmap_array[found_iter]->register_count);
        for (unsigned int i = 0; i < variable_count; ++i)
        {
            unsigned int regnum;
            int scanret = sscanf(variable_list[i].var_name, "r%u", &regnum);
            parser_printf("try to find: %s\n", variable_list[i].var_name);
            if (scanret > 0 && regnum < regmap_array[found_iter]->register_count)
            {
                variable_list[i].var_pointer->val_p = &regmap_array[found_iter]->register_value[regnum];
            }
            else
            {
                error = 4;
                break;
            }
        }

        regmap_calc_from_start[found_iter] = calc_element_first;
        calc_element_first = NULL;
        calc_element_last = NULL;
    }
    else
    {
        error = 4;
    }

    parser_safe_free(&uri);
    parser_free_variables();
}

void parser_calc_to_link(char * uri, lval_t * val)
{
    parser_printf("calc_to_link\n");
    int ok = 0;
    unsigned int found_iter = 0;
    for (unsigned int i = 0; i < regmap_count; ++i)
    {
        if (strcmp(uri, regmap_array[i]->uri) == 0)
        {
            if (val->calc_element->argc == regmap_array[i]->register_count)
            {
                found_iter = i;
                ok = 1;
            }
            break;
        }
    }

    if (ok)
    {
        regmap_calc_to_start[found_iter] = calc_element_first;
        calc_element_first = NULL;
        calc_element_last = NULL;

        for (unsigned int i = 0; i < variable_count; ++i)
        {
            if (strcmp("val", variable_list[i].var_name) == 0)
            {
                variable_list[i].var_pointer->val_p = &regmap_array[found_iter]->uri_value;
            }
            else
            {
                error = 5;
            }
        }

        for (unsigned int i = 0; i < val->calc_element->argc; ++i)
        {
            val->calc_element->argv[i]->val_p->val_p = &regmap_array[found_iter]->register_value[i];
            parser_safe_free(&val->calc_element->argv[i]);
        }
        parser_safe_free(&val->calc_element);
    }
    else
    {
        error = 5;
    }

    parser_safe_free(&uri);
    parser_free_variables();
}

lval_t parser_add_variable(lval_t val)
{
    parser_printf("add var %s\n", val.s);
    lval_t lval;
    lval.lval_type = lval_var;
    lval.variant = variant_new();
    parser_inc_allocated_count("add variable");
    variable_count++;

    variable_list = parser_alloc(variable_list, sizeof(struct variable_descriptor_t) * variable_count);

    struct variable_descriptor_t * descp = (variable_list + variable_count - 1);
    descp->var_name = val.s;

    /*
    parser_printf("after add: ");
    for (unsigned int i = 0; i < variable_count; ++i)
    {
        parser_printf("%s ", variable_list[i].var_name);
    }
    parser_printf("\n");
    */

    descp->var_pointer = lval.variant;
    return lval;
}

void parser_calc_element_append(lval_t * ret, lval_t * val)
{
    parser_printf("calc_element_append\n");
    unsigned int argc;
    if (ret->calc_element == NULL)
        argc = 0;
    else
        argc = ret->calc_element->argc;

    if (val)
        argc++;

    parser_calc_element_new(&(ret->calc_element), argc);
    if (val)
    {
        parser_calc_element_set_arg(&(ret->calc_element->argv[argc - 1]), val);
    }
}

lval_t parser_calc_element_op(parser_calc_function_t func, lval_t * l, lval_t * r)
{
    parser_printf("calc_element_op\n");
    lval_t lval;
    lval.lval_type = lval_elem;
    lval.calc_element = NULL;
    parser_calc_element_new(&lval.calc_element, 2);
    parser_calc_element_sequence_add(lval.calc_element);
    parser_calc_element_set_arg(&lval.calc_element->argv[0], l);
    parser_calc_element_set_arg(&lval.calc_element->argv[1], r);
    lval.calc_element->func = func;
    return lval;
}

lval_t parser_calc_element_end(lval_t * val)
{
    parser_printf("calc_element_end\n");
    lval_t lval;
    lval.lval_type = lval_elem;
    lval.calc_element = NULL;
    parser_calc_element_new(&lval.calc_element, 1);
    parser_calc_element_sequence_add(lval.calc_element);
    parser_calc_element_set_arg(&lval.calc_element->argv[0], val);
    lval.calc_element->func = calc_nope;
    return lval;
}

lval_t parser_func_element_foo(char * name, lval_t * lval)
{
    lval->lval_type = lval_elem;
    unsigned int tocheck = 0;
    parser_printf("foo: %s\n", name);
    if (strcmp(name, "invert") == 0)
    {
        tocheck = 1;
        lval->calc_element->func = calc_func_invert;
    }
    else
    if (strcmp(name, "get_bits") == 0)
    {
        tocheck = 3;
        lval->calc_element->func = calc_func_get_bits;
    }
    else
    if (strcmp(name, "combine_regs_2") == 0)
    {
        tocheck = 3;
        lval->calc_element->func = calc_func_combine_regs_2;
    }
    else
    if (strcmp(name, "combine_regs_4") == 0)
    {
        tocheck = 5;
        lval->calc_element->func = calc_func_combine_regs_4;
    }
    else
    {
        error = 1;
    }
    if (tocheck != lval->calc_element->argc)
    {
        error = 2;
    }
    parser_calc_element_sequence_add(lval->calc_element);
    parser_safe_free(&name);
    return *lval;
}

//------------------------------------------------------------------

void calc_nope(parser_calc_element_t * calcp)
{
    parser_printf("_ ");
    calcp->ret = *variant_dereference(calcp->argv[0]);
}

void calc_cast_type(parser_calc_element_t * calcp)
{
    parser_printf("c ");
    variant_t * p;
    p = variant_dereference(&calcp->ret);
    *p = variant_cast_to_type(variant_dereference(calcp->argv[0]), p->val_type);
}

void calc_add(parser_calc_element_t * calcp)
{
    parser_printf("+ ");
    variant_op(variant_add, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_minus(parser_calc_element_t * calcp)
{
    parser_printf("- ");
    variant_op(variant_minus, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_mul(parser_calc_element_t * calcp)
{
    parser_printf("* ");
    variant_op(variant_mul, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_div(parser_calc_element_t * calcp)
{
    parser_printf("/ ");
    variant_op(variant_div, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_mod(parser_calc_element_t * calcp)
{
    parser_printf("%% ");
    variant_op(variant_mod, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_and(parser_calc_element_t * calcp)
{
    parser_printf("& ");
    variant_op(variant_and, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_or(parser_calc_element_t * calcp)
{
    parser_printf("| ");
    variant_op(variant_or, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_xor(parser_calc_element_t * calcp)
{
    parser_printf("^ ");
    variant_op(variant_xor, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_shl(parser_calc_element_t * calcp)
{
    parser_printf("<<");
    variant_op(variant_shl, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

void calc_shr(parser_calc_element_t * calcp)
{
    parser_printf(">>");
    variant_op(variant_shr, &calcp->ret, calcp->argv[0], calcp->argv[1]);
}

/*
void calc_pwr(func_element_t * calcp)
{
}

void calc_func_swap_bytes(func_element_t * calcp)
{
}
*/

void calc_func_invert(parser_calc_element_t * calcp)
{
    parser_printf("~ ");
    variant_zero(&calcp->ret);
    calcp->ret.val.val_uint64 = ~(uint64_t)variant_dereference(calcp->argv[0])->val.val_uint64;
    calcp->ret.val_type = variant_dereference(calcp->argv[0])->val_type;
    variant_clean(&calcp->ret);
}

void calc_func_get_bits(parser_calc_element_t * calcp)
{
    parser_printf("b ");
    variant_zero(&calcp->ret);
    uint64_t val = variant_dereference(calcp->argv[0])->val.val_uint64;
    uint64_t bit = variant_dereference(calcp->argv[1])->val.val_uint64;
    uint64_t count = variant_dereference(calcp->argv[2])->val.val_uint64;
    val >>= bit;
    calcp->ret.val.val_uint64 = val & ~((uint64_t)0xFFFFFFFFFFFFFFFF << count);
    /*
    uint64_t ret_iter = 0;
    while(count-- > 0)
    {
        if (val & 1)
            calcp->ret.val.val_uint64 |= (uint64_t)1 << ret_iter;
        val >>= 1;
        ret_iter++;
    }
    */
    calcp->ret.val_type = variant_uint64;
}

void calc_func_combine_regs_2(parser_calc_element_t * calcp)
{
    parser_printf("c2");
    variant_zero(&(calcp->ret));
    calcp->ret.val.val_uint64 =
            (uint64_t)variant_dereference(calcp->argv[1])->val.val_uint16 << 16 |
            (uint64_t)variant_dereference(calcp->argv[2])->val.val_uint16;
    uint64_t arg_type = variant_dereference(calcp->argv[0])->val.val_uint64;
    enum variant_type_t type;
    if (arg_type == 2)
        type = variant_float;
    else if (arg_type == 1)
        type = variant_uint32;
    else
        type = variant_int32;
    calcp->ret.val_type = type;
}

void calc_func_combine_regs_4(parser_calc_element_t * calcp)
{
    parser_printf("c4");
    variant_zero(&calcp->ret);
    calcp->ret.val.val_uint64 =
            (uint64_t)variant_dereference(calcp->argv[1])->val.val_uint16 << 48 |
            (uint64_t)variant_dereference(calcp->argv[2])->val.val_uint16 << 32 |
            (uint64_t)variant_dereference(calcp->argv[3])->val.val_uint16 << 16 |
            (uint64_t)variant_dereference(calcp->argv[4])->val.val_uint16;
    uint64_t arg_type = variant_dereference(calcp->argv[0])->val.val_uint64;
    enum variant_type_t type;
    if (arg_type == 2)
        type = variant_double;
    else if (arg_type == 1)
        type = variant_uint64;
    else
        type = variant_int64;
    calcp->ret.val_type = type;
}
