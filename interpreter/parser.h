
#ifndef _PARSER_H_
#define _PARSER_H_

#include "variant/variant.h"


typedef struct parser_calc_element_t parser_calc_element_t;
typedef struct lval_t lval_t;
typedef void (*parser_calc_function_t)(parser_calc_element_t * func_element);


struct parser_calc_element_t
{
    parser_calc_element_t * next_element; //pointer to the next element
    parser_calc_function_t func; //the function gets an args by itself
    variant_t ret;
    unsigned int argc;
    variant_t * argv[];
};

enum lval_type_t
{
    lval_elem,
    lval_var,
};

struct lval_t
{
    enum lval_type_t lval_type;
    union
    {
        parser_calc_element_t * calc_element;
        variant_t * variant;
        char * s;
    };
};

void parser_clear_error(void);
int parser_iserror(void);
void parser_free(void);

lval_t parser_make_int(char * str);
lval_t parser_make_uint(char * str);
lval_t parser_make_double(char * str);
lval_t parser_make_str(char * str);

void parser_regmap_add(lval_t * basereg, lval_t * count, char * regtype, char * uri, char * uritype);
void parser_calc_from_link(char * uri, lval_t * val);
void parser_calc_to_link(char * uri, lval_t * val);

lval_t parser_add_variable(lval_t val);
void parser_calc_element_append(lval_t * ret, lval_t * val);
lval_t parser_calc_element_op(parser_calc_function_t func, lval_t * l, lval_t * r);
lval_t parser_calc_element_end(lval_t * lval);
lval_t parser_func_element_foo(char * name, lval_t * lval);

//----------------------------------------------------------------------------------

void calc_nope(parser_calc_element_t * calcp);
void calc_cast_type(parser_calc_element_t * calcp);

void calc_add(parser_calc_element_t * calcp);
void calc_minus(parser_calc_element_t * calcp);
void calc_mul(parser_calc_element_t * calcp);
void calc_div(parser_calc_element_t * calcp);
void calc_mod(parser_calc_element_t * calcp);
void calc_and(parser_calc_element_t * calcp);
void calc_or(parser_calc_element_t * calcp);
void calc_xor(parser_calc_element_t * calcp);
void calc_shl(parser_calc_element_t * calcp);
void calc_shr(parser_calc_element_t * calcp);
//void calc_pwr(func_element_t * calcp);
//void calc_func_swap_bytes(func_element_t * calcp);
void calc_func_invert(parser_calc_element_t * calcp);
void calc_func_get_bits(parser_calc_element_t * calcp);
void calc_func_combine_regs_2(parser_calc_element_t * calcp);
void calc_func_combine_regs_4(parser_calc_element_t * calcp);

#endif //_PARSER_H_
