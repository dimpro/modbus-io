
%option warn
%option reentrant
%option noyywrap
%option yylineno

%{
/*
%option warn
%option bison-bridge
%option bison-locations
%option reentrant
%option noyywrap
%option ecs
%option stack
%option yylineno
%option nodefault
for c++
    add %option c++
    remove reentrant, bison-bridge and bison-locations
    change all tokens like the following: LPAREN to yy::parser::token::LPAREN
*/
#include "grammar.h"
%}

%%
[/][/].*                ;
["].*["]                { yylval = parser_make_str(yytext); return STR; }
[0-9]+[.][0-9]+         { yylval = parser_make_double(yytext); return DOUBLE; }
[0][xX][a-fA-F0-9]+     { yylval = parser_make_uint(yytext); return HEXLONG; }
[0-9]+                  { yylval = parser_make_int(yytext); return LONG; }
[a-zA-Z][a-zA-Z0-9_]*   { yylval = parser_make_str(yytext); return NAME; }

"<<"                    { return SHL; }
">>"                    { return SHR; }
"**"                    { return NUM_PWR; }

";"                     { return ';'; }
","                     { return ','; }
":"                     { return ':'; }
"("                     { return '('; }
")"                     { return ')'; }
"<"                     { return '<'; }
">"                     { return '>'; }
"-"                     { return '-'; }
"+"                     { return '+'; }
"*"                     { return '*'; }
"/"                     { return '/'; }
"%"                     { return '%'; }
"&"                     { return '&'; }
"|"                     { return '|'; }
"^"                     { return '^'; }

\n                      { return EOL; }
[ \t\v\f\r]+            ;
.                       ;
%%

