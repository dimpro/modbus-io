
#include "modbus_packer.h"

#include <string.h>


enum swap_t {
    MODBUS_NOSWAP,
    MODBUS_SWAP
};

void cpy_bytes(void *dist, void *source, uint8_t length, enum swap_t swap)
{
    uint8_t *distp = dist;
    uint8_t *sourcep = source;

    if ((length % 2) || (length < 2))
        swap = MODBUS_NOSWAP;

    int swap_iter = 0;
    for (int i = 0; i < length; i++)
    {
        if (swap)
        {
            if (swap_iter != 1)
            {
                swap_iter = 1;
            }
            else
            {
                swap_iter = -1;
            }
        }
        distp[i] = sourcep[i + swap_iter];
    }
}

void pack_message(enum modbus_direction_t modbus_direction, modbus_pack_t *mpack, uint8_t *buff_out, uint8_t *size)
{
    uint8_t len = 0;

    //упаковка пакета
    len += 2;
    cpy_bytes(buff_out + 0, &(mpack->addr_device), 1, MODBUS_NOSWAP);
    cpy_bytes(buff_out + 1, &(mpack->func), 1, MODBUS_NOSWAP);

    if (modbus_direction) { //пакет для запроса
        len += 4;
        cpy_bytes(buff_out + 2, &(mpack->start_addr), 2, MODBUS_SWAP);
        cpy_bytes(buff_out + 4, &(mpack->quantity), 2, MODBUS_SWAP);

        switch (mpack->func) {
        case READ_COILS:;
        case READ_DISCRETE_INPUTS:;
        case READ_HOLDING_REGISTERS:;
        case READ_INPUT_REGISTERS:
            break;
        case WRITE_SINGLE_COIL:;
        case WRITE_SINGLE_REGISTER:
            len += -2 + 2;
            cpy_bytes(buff_out + 4, mpack->value, 2, MODBUS_SWAP);
            break;
        case WRITE_MULTIPLE_COIL:
            len += 1 + mpack->byte_count;
            cpy_bytes(buff_out + 6, &(mpack->byte_count), 1, MODBUS_NOSWAP);
            cpy_bytes(buff_out + 7, mpack->value, mpack->byte_count, MODBUS_NOSWAP);
            break;
        case WRITE_MULTIPLE_REGISTER:
            len += 1 + mpack->byte_count;
            cpy_bytes(buff_out + 6, &(mpack->byte_count), 1, MODBUS_NOSWAP);
            cpy_bytes(buff_out + 7, mpack->value, mpack->byte_count, MODBUS_SWAP);
            break;
        case READ_WRITE_MULTIPLE_REGISTERS:
            len += 5 + mpack->byte_count;
            cpy_bytes(buff_out + 6, &(mpack->start_addr), 2, MODBUS_SWAP);
            cpy_bytes(buff_out + 8, &(mpack->quantity), 2, MODBUS_SWAP);
            cpy_bytes(buff_out + 9, &(mpack->byte_count), 1, MODBUS_NOSWAP);
            cpy_bytes(buff_out + 10, mpack->value, mpack->byte_count, MODBUS_SWAP);
            break;
        case READ_DEVICE_IDENTIFICATION:
            len += -4 + 3;
            cpy_bytes(buff_out + 2, &(mpack->identification.MEI_type), 1, MODBUS_NOSWAP);
            cpy_bytes(buff_out + 3, &(mpack->identification.read_device_id_code), 1, MODBUS_NOSWAP);
            cpy_bytes(buff_out + 4, &(mpack->identification.object_id_request), 1, MODBUS_NOSWAP);
            break;
        }
    } else { //пакет для ответа
        //проверка наличия ошибки
        if (mpack->func & 0x80) {
            len += 1;
            cpy_bytes(buff_out + 2, &(mpack->exception_code), 1, MODBUS_NOSWAP);
        } else {
            switch (mpack->func) {
            case READ_COILS:;
            case READ_DISCRETE_INPUTS:
                len += 1 + mpack->byte_count;
                cpy_bytes(buff_out + 2, &(mpack->byte_count), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 3, mpack->value, mpack->byte_count, MODBUS_NOSWAP);
                break;
            case READ_HOLDING_REGISTERS:;
            case READ_INPUT_REGISTERS:;
            case REPORT_SLAVE_ID:;
            case READ_WRITE_MULTIPLE_REGISTERS:
                len += 1 + mpack->byte_count;
                cpy_bytes(buff_out + 2, &(mpack->byte_count), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 3, mpack->value, mpack->byte_count, MODBUS_SWAP);
                break;
            case WRITE_SINGLE_COIL:;
            case WRITE_SINGLE_REGISTER:
                len += 4;
                cpy_bytes(buff_out + 2, &(mpack->start_addr), 2, MODBUS_SWAP);
                cpy_bytes(buff_out + 4, mpack->value, 2, MODBUS_SWAP);
                break;
            case WRITE_MULTIPLE_COIL:;
            case WRITE_MULTIPLE_REGISTER:
                len += 4;
                cpy_bytes(buff_out + 2, &(mpack->start_addr), 2, MODBUS_SWAP);
                cpy_bytes(buff_out + 4, &(mpack->quantity), 2, MODBUS_SWAP);
                break;
            case READ_DEVICE_IDENTIFICATION:
                len += 6;
                cpy_bytes(buff_out + 2, &(mpack->identification.MEI_type), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 3, &(mpack->identification.read_device_id_code), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 4, &(mpack->identification.conformity_level), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 5, &(mpack->identification.more_follows), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 6, &(mpack->identification.next_object_id), 1, MODBUS_NOSWAP);
                cpy_bytes(buff_out + 7, &(mpack->identification.number_of_objects), 1, MODBUS_NOSWAP);
                for (int i = 0; i < mpack->identification.number_of_objects; i++) {
                    len += 2 + mpack->identification.o_list[i].object_length;
                    cpy_bytes(buff_out + 8 + i, &(mpack->identification.o_list[i].object_id), 1, MODBUS_NOSWAP);
                    cpy_bytes(buff_out + 8 + i + 1, &(mpack->identification.o_list[i].object_length), 1, MODBUS_NOSWAP);
                    cpy_bytes(buff_out + 8 + i + 2, mpack->identification.o_list[i].o_value, mpack->identification.o_list[i].object_length, MODBUS_NOSWAP);
                }
                break;
            }
        }
    }

    *size = len;
}

void unpack_message(enum modbus_direction_t modbus_direction, modbus_pack_t *mpack, uint8_t *buff_in, uint8_t *size)
{
    memset(mpack, 0, sizeof(modbus_pack_t));

    //разбор пакета
    cpy_bytes(&(mpack->addr_device), buff_in + 0, 1, MODBUS_NOSWAP);
    cpy_bytes(&(mpack->func), buff_in + 1, 1, MODBUS_NOSWAP);

    if (modbus_direction) { //нужно разобрать пакет как "запрос"
        if (mpack->func != READ_WRITE_MULTIPLE_REGISTERS) {
            cpy_bytes(&(mpack->start_addr), buff_in + 2, 2, MODBUS_SWAP);
            cpy_bytes(&(mpack->quantity), buff_in + 4, 2, MODBUS_SWAP);
        }

        switch (mpack->func) {
        case READ_COILS:;
        case READ_DISCRETE_INPUTS:;
        case READ_HOLDING_REGISTERS:;
        case READ_INPUT_REGISTERS:
            break;
        case WRITE_SINGLE_COIL:;
        case WRITE_SINGLE_REGISTER:
            mpack->quantity = 0;
            cpy_bytes(mpack->value, buff_in + 4, 2, MODBUS_SWAP);
            break;
        case WRITE_MULTIPLE_COIL:
            cpy_bytes(&(mpack->byte_count), buff_in + 6, 1, MODBUS_NOSWAP);
            cpy_bytes(mpack->value, buff_in + 7, mpack->byte_count, MODBUS_SWAP);
            break;
        case WRITE_MULTIPLE_REGISTER:
            cpy_bytes(&(mpack->byte_count), buff_in + 6, 1, MODBUS_NOSWAP);
            cpy_bytes(mpack->value, buff_in + 7, mpack->byte_count, MODBUS_SWAP);
            break;
        case REPORT_SLAVE_ID:
            break;
        case READ_WRITE_MULTIPLE_REGISTERS:
            cpy_bytes(&(mpack->start_addr), buff_in + 6, 2, MODBUS_SWAP);
            cpy_bytes(&(mpack->quantity), buff_in + 8, 2, MODBUS_SWAP);
            cpy_bytes(&(mpack->byte_count), buff_in + 9, 1, MODBUS_NOSWAP);
            cpy_bytes(mpack->value, buff_in + 10, mpack->byte_count, MODBUS_SWAP);
            break;
        case READ_DEVICE_IDENTIFICATION:
            cpy_bytes(&(mpack->identification.MEI_type), buff_in + 2, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.read_device_id_code), buff_in + 3, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.object_id_request), buff_in + 4, 1, MODBUS_NOSWAP);
            break;
        default:
            mpack->err = 1;
        }
    } else { //разобрать пакет как "ответ"
        //проверка на наличие в пакете результатов исключительной ситуации(у слейва произошла ошибка)
        if (mpack->func & 0x80) {
            cpy_bytes(&(mpack->exception_code), buff_in + 2, 1, MODBUS_NOSWAP);
            mpack->err = 1;
            return;
        }

        switch (mpack->func) {
        case READ_COILS:;
        case READ_DISCRETE_INPUTS:
            cpy_bytes(&(mpack->byte_count), buff_in + 2, 1, MODBUS_NOSWAP);
            cpy_bytes(mpack->value, buff_in + 3, mpack->byte_count, MODBUS_NOSWAP);
            break;
        case READ_HOLDING_REGISTERS:;
        case READ_INPUT_REGISTERS:;
        case READ_WRITE_MULTIPLE_REGISTERS:
            cpy_bytes(&(mpack->byte_count), buff_in + 2, 1, MODBUS_NOSWAP);
            cpy_bytes(mpack->value, buff_in + 3, mpack->byte_count, MODBUS_SWAP);
            break;
        case WRITE_SINGLE_COIL:;
        case WRITE_SINGLE_REGISTER:
            cpy_bytes(&(mpack->start_addr), buff_in + 2, 2, MODBUS_SWAP);
            cpy_bytes(mpack->value, buff_in + 4, 2, MODBUS_SWAP);
            break;
        case WRITE_MULTIPLE_COIL:;
        case WRITE_MULTIPLE_REGISTER:
            cpy_bytes(&(mpack->start_addr), buff_in + 2, 2, MODBUS_SWAP);
            cpy_bytes(&(mpack->quantity), buff_in + 4, 2, MODBUS_SWAP);
            break;
        case READ_DEVICE_IDENTIFICATION:
            cpy_bytes(&(mpack->identification.MEI_type), buff_in + 2, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.read_device_id_code), buff_in + 3, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.conformity_level), buff_in + 4, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.more_follows), buff_in + 5, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.next_object_id), buff_in + 6, 1, MODBUS_NOSWAP);
            cpy_bytes(&(mpack->identification.number_of_objects), buff_in + 7, 1, MODBUS_NOSWAP);
            for (int i = 0; i < mpack->identification.number_of_objects; i++) {
                cpy_bytes(&(mpack->identification.o_list[i].object_id), buff_in + 8 + i, 1, MODBUS_NOSWAP);
                cpy_bytes(&(mpack->identification.o_list[i].object_length), buff_in + 8 + i + 1, 1, MODBUS_NOSWAP);
                cpy_bytes(mpack->identification.o_list[i].o_value, buff_in + 8 + i + 2, mpack->identification.o_list[i].object_length, MODBUS_NOSWAP);
            }
            break;
        default:
            mpack->err = 1;
        }
    }
}
