
#ifndef _modbus_h_
#define _modbus_h_

#include <stdint.h>

#define O_VALUE_SIZE 50
#define O_LIST_SIZE 20

#ifdef __cplusplus
extern "C" {
#endif

enum modbus_direction_t {
    MODBUS_RESPONSE,
    MODBUS_REQUEST
};

enum {
	READ_COILS = 0x01,
	READ_DISCRETE_INPUTS,
	READ_HOLDING_REGISTERS,
	READ_INPUT_REGISTERS,
	WRITE_SINGLE_COIL,
	WRITE_SINGLE_REGISTER,
	WRITE_MULTIPLE_COIL = 0x0F,
	WRITE_MULTIPLE_REGISTER,
	REPORT_SLAVE_ID,
	READ_WRITE_MULTIPLE_REGISTERS = 0x17,
	READ_DEVICE_IDENTIFICATION = 0x2B,
	
	USER_DEF_CMD_READ_PR = 0x42,
	USER_DEF_CMD_WRITE_PR = 0x43
};

typedef struct modbus_pack_t {
	uint8_t err;
	uint8_t addr_device;
	uint8_t func;
	
	uint16_t start_addr;
	uint16_t quantity;
	
	uint16_t start_addr_w;
	uint16_t quantity_w;
	
	uint8_t byte_count;
	union {
		uint8_t value[O_LIST_SIZE * O_VALUE_SIZE];
		struct {
			uint8_t MEI_type;
			uint8_t read_device_id_code;
			uint8_t object_id_request;
			uint8_t conformity_level;
			uint8_t more_follows;
			uint8_t next_object_id;
			uint8_t number_of_objects;
			struct {
				uint8_t object_id;
				uint8_t object_length;
				uint8_t o_value[O_VALUE_SIZE];
			} o_list [O_LIST_SIZE];
		} identification;
	};
	
	uint8_t exception_code;
} modbus_pack_t;

/* mpack - данные для упаковки по протоколу modbus rtu
 * buff_out - буфер с выходными данными(тут будет лежать укомплектованный пакет, готовый к передаче)
 */
void pack_message(enum modbus_direction_t modbus_direction, modbus_pack_t *mpack, uint8_t *buff_out, uint8_t *size);

/* mpack - структура для распаковки в нее пакета
 * buff_in - ссылка на буфер с пакетом
 */ 
void unpack_message(enum modbus_direction_t modbus_direction, modbus_pack_t *mpack, uint8_t *buff_in, uint8_t *size);

#ifdef __cplusplus
}
#endif

#endif //_modbus_h_
