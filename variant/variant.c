
#include <stdio.h>
//#include <string.h>
#include <stdlib.h>
#include "variant.h"

#define VARIANT_FUNCTION(SIGN, SUFFIX, TYPE) \
    void variant_##SUFFIX##_##TYPE(variant_t * ret, variant_t * l, variant_t * r) { \
        ret->val.val_##TYPE = l->val.val_##TYPE SIGN r->val.val_##TYPE; }

#define VARIANT_FUNCTION_CHECK_ZERO(SIGN, SUFFIX, TYPE) \
    void variant_##SUFFIX##_##TYPE(variant_t * ret, variant_t * l, variant_t * r) { \
        if (r->val.val_uint64 == 0) return; \
        ret->val.val_##TYPE = l->val.val_##TYPE SIGN r->val.val_##TYPE; }

#define VARIANT_SET_MAP_ITEM(SUFFIX, TYPE) \
    &variant_##SUFFIX##_##TYPE,

#define EMPTY(...)
#define DEFER(...) __VA_ARGS__ EMPTY()
#define OBSTRUCT(...) __VA_ARGS__ DEFER(EMPTY)()
#define EXPAND(...) __VA_ARGS__

#define VARIANT_APPLY_DEF(DEF, ...) EXPAND(DEFER(DEF)(__VA_ARGS__))

#define VARIANT_CREATE_OPERATION(DEF_INT, DEF_FLOAT, ...) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, int8) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, uint8) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, int16) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, uint16) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, int32) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, uint32) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, int64) \
    VARIANT_APPLY_DEF(DEF_INT, __VA_ARGS__, uint64) \
    VARIANT_APPLY_DEF(DEF_FLOAT, __VA_ARGS__, float) \
    VARIANT_APPLY_DEF(DEF_FLOAT, __VA_ARGS__, double)

VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, VARIANT_FUNCTION, +, add)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, VARIANT_FUNCTION, -, minus)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, VARIANT_FUNCTION, *, mul)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION_CHECK_ZERO, VARIANT_FUNCTION_CHECK_ZERO, /, div)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION_CHECK_ZERO, EMPTY, %, mod)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, EMPTY, &, and)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, EMPTY, |, or)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, EMPTY, ^, xor)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, EMPTY, <<, shl)
VARIANT_CREATE_OPERATION(VARIANT_FUNCTION, EMPTY, >>, shr)

typedef void(*variant_op_func)(variant_t*,variant_t*,variant_t*);
static variant_op_func variant_op_map[variant_op_count][variant_type_count] =
{
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, VARIANT_SET_MAP_ITEM, add)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, VARIANT_SET_MAP_ITEM, minus)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, VARIANT_SET_MAP_ITEM, mul)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, VARIANT_SET_MAP_ITEM, div)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, EMPTY, mod)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, EMPTY, and)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, EMPTY, or)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, EMPTY, xor)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, EMPTY, shl)},
    {VARIANT_CREATE_OPERATION(VARIANT_SET_MAP_ITEM, EMPTY, shr)}
};

static uint64_t variant_bits_in_type[variant_type_count] =
{
    UINT8_MAX,
    UINT8_MAX,
    UINT16_MAX,
    UINT16_MAX,
    UINT32_MAX,
    UINT32_MAX,
    UINT64_MAX,
    UINT64_MAX,
    UINT64_MAX,
    UINT64_MAX
};

#define BIT(x) ((uint64_t)1 << x)
static uint64_t variant_sign_check[variant_type_count][variant_type_count] =
{
    {0, 0, BIT(7), 0, BIT(7), 0, BIT(7), 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, BIT(15), 0, BIT(15), 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, BIT(31), 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

#define SIGN_BITS(BIGGER, SMALLER) (((uint64_t)UINT##BIGGER##_MAX & (~(uint64_t)UINT##SMALLER##_MAX)))
static uint64_t variant_sign_set[variant_type_count][variant_type_count] =
{
    {0, 0, SIGN_BITS(16,8), 0, SIGN_BITS(32,8), 0, SIGN_BITS(64,8), 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, SIGN_BITS(32,16), 0, SIGN_BITS(64,16), 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, SIGN_BITS(64,32), 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

void variant_cast_int64_double(variant_t * v, enum variant_type_t type) {v->val.val_double = (double)v->val.val_int64; v->val_type = variant_double;}
void variant_cast_double_float(variant_t * v, enum variant_type_t type) {v->val.val_float = (float)v->val.val_double; v->val_type = variant_float;}

void variant_cast_float_double(variant_t * v, enum variant_type_t type) {v->val.val_double = (double)v->val.val_float; v->val_type = variant_double;}
void variant_cast_double_int64(variant_t * v, enum variant_type_t type) {v->val.val_int64 = (int64_t)v->val.val_double; v->val_type = variant_int64;}

void variant_cast_intx_intx(variant_t * v, enum variant_type_t type)
{
    if (v->val.val_uint64 & variant_sign_check[v->val_type][type])
        v->val.val_uint64 |= variant_sign_set[v->val_type][type];
    v->val_type = type;
}
void variant_cast_intx_int64(variant_t * v, enum variant_type_t type) {variant_cast_intx_intx(v, variant_int64);}

#define VC(x,y) variant_cast_##x##_##y
typedef void (*variant_cast_func)(variant_t*, enum variant_type_t);
static variant_cast_func variant_cast_func_map[variant_type_count][variant_type_count] =
{
    //i8
    {0, VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,int64), VC(intx,int64)},
    //u8
    {VC(intx,intx), 0, VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,int64), VC(intx,int64)},
    //i16
    {VC(intx,intx), VC(intx,intx), 0, VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,int64), VC(intx,int64)},
    //u16
    {VC(intx,intx), VC(intx,intx), VC(intx,intx), 0, VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,int64), VC(intx,int64)},
    //i32
    {VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), 0, VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,int64), VC(intx,int64)},
    //u32
    {VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), 0, VC(intx,intx), VC(intx,intx), VC(intx,int64), VC(intx,int64)},
    //i64
    {VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), 0, VC(intx,intx), VC(int64,double), VC(int64,double)},
    //u64
    {VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), VC(intx,intx), 0, VC(intx,int64), VC(intx,int64)},
    //float
    {VC(float,double), VC(float,double), VC(float,double), VC(float,double), VC(float,double), VC(float,double), VC(float,double), VC(float,double), 0, VC(float,double)},
    //double
    {VC(double,int64), VC(double,int64), VC(double,int64), VC(double,int64), VC(double,int64), VC(double,int64), VC(double,int64), VC(double,int64), VC(double,float), 0},
};

enum variant_type_t variant_get_common_type(variant_t * l, variant_t * r)
{
    if (l->val_type > r->val_type) { return l->val_type; }
    else { return r->val_type; }
}

void variant_clean(variant_t * val)
{
    val->val.val_uint64 &= variant_bits_in_type[val->val_type];
}

variant_t variant_cast_to_type(variant_t * var, enum variant_type_t type)
{
    variant_t ret;

    ret = *var;
    variant_clean(&ret);

    while(ret.val_type != type)
        variant_cast_func_map[ret.val_type][type](&ret, type);

    return ret;
}

void variant_zero(variant_t * val)
{
    val->val.val_uint64 = 0;
    val->val_type = variant_uint64;
    val->val_p = NULL;
}

variant_t * variant_new()
{
    variant_t * ret = malloc(sizeof(variant_t));
    variant_zero(ret);
    return ret;
}

variant_t * variant_dereference(variant_t * val)
{
    if (val->val_p)
        return val->val_p;
    else
        return val;
}

int variant_op(enum variant_operation_t op, variant_t * ret, variant_t * l, variant_t * r)
{
    //variant_zero(ret);
    variant_t *retp, * lp, * rp;

    retp = variant_dereference(ret);
    lp = variant_dereference(l);
    rp = variant_dereference(r);

    enum variant_type_t type = variant_get_common_type(lp, rp);
    variant_t l_casted = variant_cast_to_type(lp, type);
    variant_t r_casted = variant_cast_to_type(rp, type);
    variant_op_map[op][type](retp, &l_casted, &r_casted);
    retp->val_type = type;
    return 0;
}
